<?php

$todos = [];

if (file_exists('todo.json')) {
    $json = file_get_contents('todo.json');
    $todos = json_decode($json, true);
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <style>
        input[type="checkbox"]{
            width: 30px;
            height: 30px;
            cursor: pointer;
        }

    </style>
    <title>Document</title>
</head>
<body class="bg-gray-700">

    <div class="h-100 w-full flex items-center justify-center bg-teal-lightest font-sans">

        <div class="bg-white rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg">

            <h1 class="text-grey-darkest">Todo list:</h1>
            <form class="mb-4" action="newTodo.php" method="post">
                <div class="flex mt-4">
                    <label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 mr-4 text-grey-darker" type="text" name="todo_name">
                    </label>
                    <button class="flex-no-shrink p-2 border-2 rounded text-teal border-teal hover:text-white hover:bg-gray-500">New Todo</button>
                </div>
            </form>

            <?php foreach ($todos as $todoName => $todo):  ?>

                <div class="flex mb-4 items-center">


                    <?php

                    echo "<p class='w-full text-grey-darkest'>${todoName}</p>"

                    ?>

                    <form class="inline-block" action="checkTodo.php" method="post">

                        <input type="hidden" name="todo_name" value="<?php echo $todoName ?>">
                        <input class="p-2 mr-2" type="checkbox" <?php echo $todo['completed'] ? 'checked' : '' ?>>

                    </form>

                    <form class="inline-block" action="delete.php" method="post">
                        <input type="hidden" name="todo_name" value="<?php echo $todoName ?>">
                        <button class="flex-no-shrink p-2 ml-2 border-2 rounded text-red border-red hover:text-white hover:bg-red-500">Delete</button>
                    </form>

                </div>

            <?php endforeach;?>
        </div>

    </div>

<script>
    const checkboxes = document.querySelectorAll('input[type=checkbox]');

    checkboxes.forEach(checkbox => {
        checkbox.onclick = function (){
            this.parentNode.submit();
        }
    })
</script>
</body>
</html>
